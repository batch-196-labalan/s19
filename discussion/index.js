// CONDITIONAL STATEMENT

/*

if(condition){
	task/code to perform
}

*/

let num1 = 0;

if(num1 === 0){
	console.log("The value of num1 is 0");
}

if(num1 === 25){
	console.log("The current value of num1 is still 0");
}


let city = "New York";

if(city === "New Jersey"){
	console.log("Welcome to New Jersey!");
}



// ELSE STATEMENT

// sample 1
if(city === "New Jersey"){
	console.log("Welcome to New Jersey!");
} else{
	console.log("This is not New Jersey!");
}


// sample 2
let no1 = 25
if(no1 < 20){
	console.log("num1's value is less than 20");
}else {
	console.log("num1's value is more than 20");
}



// sample 3
function cityChecker(city){

	if(city === "New York"){
		console.log("Welcome to Empire State!");
	} else {
		console.log("You're not in New York!");
	}
}

cityChecker("New York");
cityChecker("Los Angeles");


// MINI ACTIVITY

function budgetChecker(number){

	if(number <= 40000){
		console.log("You're still within budget!");
	} else {
		console.log("You are currently over budget");
	}
}

budgetChecker(20000);
budgetChecker(45000);



// ELSE IF STATEMENT

let city2 = "Manila"

if(city2 === "New York"){
	console.log ("Welcome to New York");
} else if(city2 === "Manila"){
	console.log("Welcome to Manila!")
} else {
	console.log("I don't know where you are.")
}

// if (city2 === "New York"){
// 	console.log("New York City");
// }


// else if(city2 === "Manila"){
// 	console.log("I keep coming back.")
// }


let role = "admin"
if(role === "developer"){
	console.log("Welcome back developer!");
}else {
	console.log("Role provided is invalid");
}


// multiple else if

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet"
	}else if ( windSpeed <= 61){
		return "Tropical Depression Detected";
	}else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected"
	}else if(windSpeed >= 89  || windSpeed <= 117){
	return "Severe storm detected"
	}else {
		return "typhoon detected"
	}
}

let typhoonMessage1 = determineTyphoonIntensity(29);
let typhoonMessage2 = determineTyphoonIntensity(62);
let typhoonMessage3 = determineTyphoonIntensity(61);
let typhoonMessage4 = determineTyphoonIntensity(88);
let typhoonMessage5 = determineTyphoonIntensity(120);


console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);



// TRUTHY & FALSY VALUES


// truthy 
// 1. true
// 2. 
if(1){
	console.log("1 is truthy");
}

// 3.
if([]){
	console.log("[] empty array is truthy")
}


// falsy
// 1. false
// 2. 0
if(0){
	console.log("0 is falsy");
}

// 3.
if(undefined){
	console.log("undefined is not falsy")
} else {
	console.log("undefined is falsy")
}



// CONDITIONAL TERNARY OPERATOR
/*
	SYNTAX: (condition) ? ifTrue : ifFalse;

*/

let age = 17;
let result = age < 18 ? "Underaged." : "Legal age";
console.log(result);



// SWITCH STATEMENT

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day) {
	case 'monday':
	console.log("color of the day is red.");
		break;
	case 'tuesday':
	console.log("color of the day is orange.");
		break;
	case 'wednesday':
	console.log("color of the day is yellow.");
		break;
	default:
		console.log("Please enter a valid day");
		break;
}



// MINI ACTIVITY

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day) {
// 	case 'monday':
// 	console.log("color of the day is red.");
// 		break;
// 	case 'tuesday':
// 	console.log("color of the day is orange.");
// 		break;
// 	case 'wednesday':
// 	console.log("color of the day is yellow.");
// 		break;
// 	case 'thursday':
// 	console.log("It's a beautiful day to be with you.");
// 		break;
// 	case 'friday':
// 	console.log("Today is your day claim it!");
// 		break;
// 	case 'saturday':
// 	console.log("Start your day with a cup of coffee.");
// 		break;
// 	case 'sunday':
// 	console.log("The day is as bright as you are.");
// 		break;	
// 	default:
// 		console.log("Please enter a valid day");
// 		break;						
// }



// TRY-CATCH-FINALLY

// try{

// 	alerts(determineTyphoonIntensity(50))
// } catch (error){

// 	// console.log(typeof error);
// 	// console.log(error);
// 	console.log(error.message);
// } finally {
// 	alert("Intensity updates will show in a new alert");

// }

// console.log("Will we continue to the next code?");

// // ----------
// try{

// 	alert(determineTyphoonIntensity(50))
// } catch (error){

// 	// console.log(typeof error);
// 	// console.log(error);
// 	console.log(error.message);
// } finally {
// 	alert("Intensity updates will show in a new alert");

// }

// console.log("Will we continue to the next code?");
